CREATE DATABASE IF NOT EXISTS `photo_contest_db`;
USE `photo_contest_db`;

create or replace table categories
(
    category_id int auto_increment
        primary key,
    category_type varchar(50) not null,
    constraint categories_category_type_uindex
        unique (category_type)
);

create or replace table roles
(
    role_id int auto_increment
        primary key,
    role_type varchar(40) not null
);

create or replace table users
(
    user_id int auto_increment
        primary key,
    username varchar(20) not null,
    first_name varchar(20) not null,
    last_name varchar(20) not null,
    password text not null,
    is_active tinyint(1) default 1 not null,
    constraint users_username_uindex
        unique (username)
);

create or replace table photos
(
    photo_id         int auto_increment
        primary key,
    source           text                 not null,
    user_id          int                  not null,
    is_contest_photo tinyint(1) default 0 not null,
    title            varchar(40)          not null,
    constraint photos_users_fk
        foreign key (user_id) references photo_contest_db.users (user_id)
);

create or replace table contests
(
    contest_id     int auto_increment
        primary key,
    title          varchar(50)          not null,
    is_open        tinyint(1) default 1 not null,
    category_id    int                  not null,
    phase_I_time   datetime             not null,
    phase_II_time  datetime             not null,
    cover_photo_id int                  null,
    is_finished    tinyint(1) default 0 not null,
    constraint contests_title_uindex
        unique (title),
    constraint contests_category__fk
        foreign key (category_id) references photo_contest_db.categories (category_id),
    constraint contests_photos_fk
        foreign key (cover_photo_id) references photo_contest_db.photos (photo_id)
);



create or replace table contests_jury
(
    contest_id int not null,
    user_id int not null,
    constraint contests_jury_contest_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contests_jury_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table invited_junkies
(
    contest_id int not null,
    user_id int not null,
    constraint invited_junkies_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint invited_junkies_users_fk
        foreign key (user_id) references users (user_id)
);
create or replace table contest_entry
(
    entry_id int auto_increment
        primary key,
    user_id int not null,
    title tinytext not null,
    story text not null,
    photo_id int null,
    contest_id int not null,
    constraint contest_entry_contests_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contest_entry_photos_fk
        foreign key (photo_id) references photos (photo_id),
    constraint contest_entry_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table reviews
(
    review_id int auto_increment
        primary key,
    comment text not null,
    score tinyint not null,
    entry_id int not null,
    created_by int not null,
    constraint reviews_entry_fk
        foreign key (entry_id) references contest_entry (entry_id)
);

create or replace table user_roles
(
    user_id int not null,
    role_id int not null,
    constraint user_roles_roles_fk
        foreign key (role_id) references roles (role_id),
    constraint user_roles_users_fk
        foreign key (user_id) references users (user_id)
);

create or replace table user_score
(
    id          int auto_increment
        primary key,
    user_id     int not null,
    score_total int not null,
    constraint user_score_and_rank_user_id_uindex
        unique (user_id),
    constraint user_score_and_rank_users_fk
        foreign key (user_id) references photo_contest_db.users (user_id)
);






