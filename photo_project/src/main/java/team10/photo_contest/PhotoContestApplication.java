package team10.photo_contest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import team10.photo_contest.config.FileStorageProperties;

@EnableConfigurationProperties({
        FileStorageProperties.class
})

@SpringBootApplication
public class PhotoContestApplication {

    public static void main(String[] args) {

        SpringApplication.run(PhotoContestApplication.class, args);

    }

}
