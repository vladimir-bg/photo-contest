package team10.photo_contest.services.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.UnauthorizedOperationException;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.*;

import static team10.photo_contest.exceptions.ErrorMessages.*;

@Service
public class VerificationHelperImpl implements VerificationHelper {
    private final UserRepository userRepository;
    private final ReviewsRepository reviewsRepository;
    private final ContestRepository contestRepository;
    private final EntryRepository entryRepository;

    @Autowired
    public VerificationHelperImpl(UserRepository userRepository,
                                  ReviewsRepository reviewsRepository,
                                  ContestRepository contestRepository, EntryRepository entryRepository) {
        this.userRepository = userRepository;
        this.reviewsRepository = reviewsRepository;
        this.contestRepository = contestRepository;
        this.entryRepository = entryRepository;
    }

    public void verifyUserCanView(int userToModifyId, User user) {
        User userToModify = userRepository.getById(userToModifyId);
        if (!userToModify.getUserName().equals(user.getUserName())) {
            throw new UnauthorizedOperationException(PHOTO_MODIFY);
        }
    }

    public void verifyJuryCanModifyReview(Review review, User user) {
        User reviewCreator = userRepository.getById(reviewsRepository.getById(review.getReviewId()).getCreatedBy());
        if (!user.getUserName().equals(reviewCreator.getUserName())) {
            throw new UnauthorizedOperationException(REVIEW_MODIFY);
        }
    }

    public void verifyUserCanModifyEntry(Entry entry, User user) {
        User entryCreator = userRepository.getById(entryRepository.getById(entry.getEntryId()).getUser().getUserId());
        if (!user.getUserName().equals(entryCreator.getUserName())) {
            throw new UnauthorizedOperationException(PHOTO_MODIFY);
        }
    }

    public void checkIfOrganizer(User user) {
        if (!user.isOrganizer()) {
            throw new UnauthorizedOperationException(ACTION_FOR_ORGANIZERS);
        }
    }

    public Contest checkIfContestExists(int contestId) {
        try {
            return contestRepository.getById(contestId);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }
}

