package team10.photo_contest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team10.photo_contest.dtos.output.RankDto;
import team10.photo_contest.enums.Ranks;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.NotAllowedOperationException;
import team10.photo_contest.models.User;
import team10.photo_contest.models.UserScore;
import team10.photo_contest.repositories.contracts.UserRepository;
import team10.photo_contest.repositories.contracts.UserScoreRepository;
import team10.photo_contest.services.contracts.UserScoreService;

import java.util.List;

import static team10.photo_contest.exceptions.ErrorMessages.SCORE_CANNOT_BE_SHOWN;
import static team10.photo_contest.exceptions.ErrorMessages.SCORE_CANNOT_BE_UPDATED;

@Service
public class UserScoreServiceImpl implements UserScoreService {
    private final int DEFAULT_SCORE = 0;

    private final UserScoreRepository userScoreRepository;
    private final UserRepository userRepository;

    @Autowired
    public UserScoreServiceImpl(UserScoreRepository userScoreRepository,
                                UserRepository userRepository) {
        this.userScoreRepository = userScoreRepository;
        this.userRepository = userRepository;
    }

    @Override
    public UserScore getById(int id) {
        return userScoreRepository.getById(id);
    }

    @Override
    public List<User> getByRank(int min, int max) {
        return userScoreRepository.getByRank(min, max);
    }

    @Override
    public void update(User user, UserScore rankAndScore) {
        try {
            userRepository.getById(user.getUserId());
            userScoreRepository.getById(rankAndScore.getId());
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        if (user.getUserId() != rankAndScore.getUser().getUserId()) {
            throw new NotAllowedOperationException(SCORE_CANNOT_BE_UPDATED);
        }
        userScoreRepository.update(rankAndScore);
    }

    @Override
    public UserScore create(User user, UserScore rankAndScore) {
        rankAndScore.setTotalScore(DEFAULT_SCORE);
        rankAndScore.setUser(user);
        userScoreRepository.create(rankAndScore);
        return rankAndScore;
    }

    @Override
    public RankDto getByUserId(int id, User user) {
        RankDto rankDto = new RankDto();
        UserScore userScore = userScoreRepository.getByUserId(id);

        if (user.getUserId() != userScore.getUser().getUserId()
                && !user.isOrganizer()) {
            throw new NotAllowedOperationException(SCORE_CANNOT_BE_SHOWN);
        }
        int score = userScore.getTotalScore();
        Ranks ranks = scoreToRank(score);
        rankDto.setCurrentRank(ranks.toString());
        rankDto.setCurrentScore(userScore.getTotalScore());
        if (!ranks.equals(Ranks.DICTATOR)) {
            rankDto.setNextScore(ranks.getNext().getLevel() - score);
            rankDto.setNextRank(ranks.getNext().toString());
        }
        return rankDto;
    }

    private Ranks scoreToRank(int score) {
        Ranks ranks = null;
        if (score >= Ranks.JUNKIE.getLevel() && score < Ranks.ENTHUSIAST.getLevel()) ranks = Ranks.JUNKIE;
        else if (score >= Ranks.ENTHUSIAST.getLevel() && score < Ranks.MASTER.getLevel()) ranks = Ranks.ENTHUSIAST;
        else if (score >= Ranks.MASTER.getLevel() && score < Ranks.DICTATOR.getLevel()) ranks = Ranks.MASTER;
        else if (score >= Ranks.DICTATOR.getLevel()) ranks = Ranks.DICTATOR;
        return ranks;
    }
}
