package team10.photo_contest.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import team10.photo_contest.dtos.output.EntryViewDto;
import team10.photo_contest.dtos.output.ReviewShowDto;
import team10.photo_contest.exceptions.ContestNotReviewableException;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.UnauthorizedOperationException;
import team10.photo_contest.mapers.ReviewModelMapper;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.ContestRepository;
import team10.photo_contest.repositories.contracts.EntryRepository;
import team10.photo_contest.repositories.contracts.ReviewsRepository;
import team10.photo_contest.services.contracts.ReviewService;
import team10.photo_contest.services.utils.VerificationHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReviewServiceImpl implements ReviewService {
    private final ReviewsRepository reviewsRepository;
    private final EntryRepository entryRepository;
    private final ContestRepository contestRepository;
    private final VerificationHelper verificationHelper;
    private final ReviewModelMapper modelMapper;

    @Autowired
    public ReviewServiceImpl(ReviewsRepository reviewsRepository,
                             EntryRepository entryRepository,
                             ContestRepository contestRepository,
                             VerificationHelper verificationHelper,
                             ReviewModelMapper modelMapper) {
        this.reviewsRepository = reviewsRepository;
        this.entryRepository = entryRepository;
        this.contestRepository = contestRepository;
        this.verificationHelper = verificationHelper;
        this.modelMapper = modelMapper;
    }

    @Override
    public ReviewShowDto create(Review review, User user) {
        Entry entry;
        Contest contest;

        try {
            entry = entryRepository.getById(review.getContestEntry().getEntryId());
            contest = contestRepository.getById(entry.getContest().getContestId());
            if (!contestRepository.isJurorInContest(contest.getContestId(), user.getUserId())) {
                throw new EntityNotFoundException("Jury with this id not found for this contest");
            }

        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        Date currentDateTime = new Date();

        if (currentDateTime.before(contest.getPhaseITime()) || currentDateTime.after(contest.getPhaseIITime())) {
            throw new ContestNotReviewableException("Contest cannot be reviewed in this phase.");
        }
        boolean isReviewed = true;
        try {
            reviewsRepository.getByEntryAndJuror(entry.getEntryId(), user.getUserId());
        } catch (EntityNotFoundException e) {
            isReviewed = false;
        }

        if (!isReviewed) {
            return modelMapper.fromObject(reviewsRepository.create(review));
        } else throw new DuplicateEntityException("Entry already reviewed by this juror.");
    }

    @Override
    public List<EntryViewDto> viewEntriesForInvitedContests(User user) {
        if (!user.isOrganizer() && user.getUserScore().getTotalScore() <= 150) {
            throw new UnauthorizedOperationException("Only invited jury members can view this information.");
        }
        List<Entry> getList = reviewsRepository.viewEntriesForInvitedContests(user);
        List<EntryViewDto> listToView = new ArrayList<>();
        for (Entry entry : getList) {
            EntryViewDto addNew = new EntryViewDto();
            addNew.setEntryId(entry.getEntryId());
            addNew.setCreatedBy(entry.getUser().getUserId());
            addNew.setEntryTitle(entry.getTitle());
            addNew.setEntryStory(entry.getStory());
            addNew.setPhotoId(entry.getPhoto().getPhotoId());
            addNew.setContestId(entry.getContest().getContestId());
            addNew.setContestCategory(entry.getContest().getCategories().getCategoryType());
            listToView.add(addNew);
        }
        return listToView;
    }

    @Override
    public ReviewShowDto update(Review review, User user) {
        verificationHelper.verifyJuryCanModifyReview(review, user);
        return modelMapper.fromObject(reviewsRepository.update(review));
    }
}
