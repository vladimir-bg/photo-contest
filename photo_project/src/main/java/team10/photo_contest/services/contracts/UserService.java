package team10.photo_contest.services.contracts;

import team10.photo_contest.models.User;

import java.util.List;

public interface UserService {
    User create(User user);
    void update(User id, User user);
    void deactivate(int id, User user);
    User getById(int id, User user);
    List<User> getAllAndSort(User user);
    User getByUsername(String username);

    int getTotalUsers();
}
