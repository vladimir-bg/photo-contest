package team10.photo_contest.services.contracts;

import team10.photo_contest.dtos.output.EntryViewDto;
import team10.photo_contest.dtos.output.ReviewShowDto;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;

import java.util.List;

public interface ReviewService {
    ReviewShowDto create(Review review, User user);
    ReviewShowDto update(Review review,User user);
    List<EntryViewDto> viewEntriesForInvitedContests(User user);
}
