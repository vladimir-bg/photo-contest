package team10.photo_contest.dtos;

public class AddPhotoDto {

    private int photoId;

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
}
