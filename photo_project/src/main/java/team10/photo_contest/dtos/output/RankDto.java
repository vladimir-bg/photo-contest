package team10.photo_contest.dtos.output;

public class RankDto {
    private String currentRank;
    private int currentScore;
    private String nextRank;
    private int nextScore;

    public String getCurrentRank() {
        return currentRank;
    }

    public void setCurrentRank(String currentRank) {
        this.currentRank = currentRank;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void setCurrentScore(int currentScore) {
        this.currentScore = currentScore;
    }

    public String getNextRank() {
        return nextRank;
    }

    public void setNextRank(String nextRank) {
        this.nextRank = nextRank;
    }

    public int getNextScore() {
        return nextScore;
    }

    public void setNextScore(int nextScore) {
        this.nextScore = nextScore;
    }
}
