package team10.photo_contest.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContestUpdateDto {
    @NotNull(message = "Username can not be null")
    @Size(min = 5, message = "Username min length is 5 symbols")
    private String userNames;

    public String getUserNames() {
        return userNames;
    }

    public void setUserNames(String userNames) {
        this.userNames = userNames;
    }
}
