package team10.photo_contest.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class PhotoFromUrlDto {

    @NotNull(message = "Photo URL cannot be null")
    private String photoUrl;

    @Size(min = 2, max = 40, message = "Photo name bust be between 2 and 40 characters")
    private String photoName;

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }
}
