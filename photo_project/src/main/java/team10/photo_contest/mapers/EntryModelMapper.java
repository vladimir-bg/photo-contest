package team10.photo_contest.mapers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team10.photo_contest.dtos.EntryDto;
import team10.photo_contest.dtos.PhotoFromUrlDto;
import team10.photo_contest.dtos.output.EntryViewDto;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Photo;
import team10.photo_contest.repositories.contracts.ContestRepository;
import team10.photo_contest.repositories.contracts.EntryRepository;
import team10.photo_contest.repositories.contracts.UserRepository;

import static team10.photo_contest.exceptions.ErrorMessages.PHOTO_EDIT_ERROR;

@Component
public class EntryModelMapper {
    private final EntryRepository entryRepository;
    private final UserRepository userRepository;
    private final ContestRepository contestRepository;

    @Autowired
    public EntryModelMapper(EntryRepository entryRepository, UserRepository userRepository,
                            ContestRepository contestRepository) {
        this.entryRepository = entryRepository;
        this.userRepository = userRepository;
        this.contestRepository = contestRepository;
    }

    public Entry fromDto(EntryDto entryDto) {
        Entry entry = new Entry();
        dtoToObject(entry, entryDto);
        return entry;
    }

    public EntryViewDto toDto(Entry entry, EntryViewDto entryViewDto) {
        objectToDto(entry,entryViewDto);
        return entryViewDto;
    }

    public EntryDto convertToEntryDto(PhotoFromUrlDto dto) {
        EntryDto entryDto = new EntryDto();
        entryDto.setPhotoName(dto.getPhotoName());
        entryDto.setPhotoUrl(dto.getPhotoUrl());
        entryDto.setContestId(0);
        return entryDto;
    }

    public Entry addPhoto(Photo photo, int id) {
        Entry entry = entryRepository.getById(id);
        addPhotoToEntry(entry, photo);
        return entry;
    }
    private void objectToDto(Entry entry, EntryViewDto entryViewDto) {
        entryViewDto.setEntryId(entry.getEntryId());
        entryViewDto.setContestId(entry.getContest().getContestId());
        entryViewDto.setContestCategory(entry.getContest().getCategories().getCategoryType());
        entryViewDto.setPhotoId(entry.getPhoto().getPhotoId());
        entryViewDto.setPhotoSource(entry.getPhoto().getSource());
        entryViewDto.setEntryTitle(entry.getTitle());
        entryViewDto.setEntryStory(entry.getStory());
        entryViewDto.setCreatedBy(entry.getUser().getUserId());
    }

    private void addPhotoToEntry(Entry entry, Photo photo) {
        if (entry.getPhoto() != null) throw new DuplicateEntityException(PHOTO_EDIT_ERROR);
        entry.setPhoto(photo);
    }

    private void dtoToObject(Entry entry, EntryDto entryDto) {
        entry.setTitle(entryDto.getTitle());
        entry.setStory(entryDto.getStory());
        entry.setUser(userRepository.getById(entryDto.getUserId()));
        entry.setContest(contestRepository.getById(entryDto.getContestId()));
    }
}
