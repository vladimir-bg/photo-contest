package team10.photo_contest.mapers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team10.photo_contest.dtos.ReviewCreateDto;
import team10.photo_contest.dtos.output.ReviewShowDto;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.EntryRepository;
import team10.photo_contest.repositories.contracts.ReviewsRepository;

@Component
public class ReviewModelMapper {

   private final EntryRepository entryRepository;
   private final ReviewsRepository reviewsRepository;
   private final String CATEGORY_IS_WRONG="The entry does not match the category!";

    @Autowired
    public ReviewModelMapper(EntryRepository entryRepository, ReviewsRepository reviewsRepository) {
        this.entryRepository = entryRepository;
        this.reviewsRepository = reviewsRepository;
    }

    public Review fromDto(ReviewCreateDto reviewCreateDto, User user) {
        Review review=new Review();
        dtoToObject(reviewCreateDto,user,review);
        return review;
    }

    public ReviewShowDto fromObject(Review review){
        ReviewShowDto reviewDto = new ReviewShowDto();
        objectToDto(reviewDto,review);
        return reviewDto;
    }

    public Review wrongCategory(int entityId, User user) {
        Review review = new Review();
        review.setComment(CATEGORY_IS_WRONG);
        review.setCreatedBy(user.getUserId());
        review.setContestEntry(entryRepository.getById(entityId));
        review.setScore(0);
        return review;
    }

    public Review fromDto(ReviewCreateDto reviewCreateDto, User user, int reviewID) {
        Review review = reviewsRepository.getById(reviewID);
        dtoToObject(reviewCreateDto, user, review);
        return review;
    }

    private void objectToDto(ReviewShowDto reviewDto, Review review){
        reviewDto.setReviewId(review.getReviewId());
        reviewDto.setComment(review.getComment());
        reviewDto.setScore(review.getScore());
        reviewDto.setEntryId(review.getContestEntry().getEntryId());
        reviewDto.setCreatedBy(review.getCreatedBy());
    }

     private void dtoToObject(ReviewCreateDto reviewCreateDto, User user, Review review) {
        Entry entry = entryRepository.getById(reviewCreateDto.getEntryId());
        review.setComment(reviewCreateDto.getComment());
        review.setCreatedBy(user.getUserId());
        review.setContestEntry(entry);
        review.setScore(reviewCreateDto.getScore());
    }
}
