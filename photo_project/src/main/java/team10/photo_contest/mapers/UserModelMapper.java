package team10.photo_contest.mapers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import team10.photo_contest.dtos.UserEditDto;
import team10.photo_contest.dtos.UserRegisterDto;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.RoleRepository;
import team10.photo_contest.repositories.contracts.UserRepository;

import java.util.Set;

@Component
public class UserModelMapper {
    private final String DEFAULT_ROLE = "Photo Junkie";
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @Autowired
    public UserModelMapper(RoleRepository roleRepository, PasswordEncoder passwordEncoder,
                           UserRepository userRepository) {
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    public User fromDto(UserRegisterDto registerDto, User user) {
        dtoToObject(registerDto, user);
        return user;
    }
    public UserEditDto toDto(User user, UserEditDto userEditDto) {
        objectToDto(user,userEditDto);
        return userEditDto;
    }

    public User fromDto(UserEditDto editDto, int id) {
        User userToUpdate = userRepository.getById(id);
        dtoToObject(editDto, userToUpdate);
        return userToUpdate;
    }
    private void objectToDto(User user,UserEditDto editDto) {
       editDto.setFirstName(user.getFirstName());
       editDto.setLastName(user.getLastName());
    }
    private void dtoToObject(UserRegisterDto registerDto, User user) {
        user.setUserName(registerDto.getUsername());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        user.setActive(true);
        user.setUserRoles(Set.of(roleRepository.getByType(DEFAULT_ROLE)));
    }

    private void dtoToObject(UserEditDto editDto, User user) {
        user.setFirstName(editDto.getFirstName());
        user.setLastName(editDto.getLastName());
        user.setPassword(passwordEncoder.encode(editDto.getPassword()));
    }
}
