package team10.photo_contest.mapers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team10.photo_contest.dtos.output.UserWithRankDto;
import team10.photo_contest.models.User;
import team10.photo_contest.services.contracts.UserScoreService;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserScoreModelMapper {
    private final UserScoreService userScoreService;
    @Autowired
    public UserScoreModelMapper(UserScoreService userScoreService) {
        this.userScoreService = userScoreService;
    }
    public List<UserWithRankDto> fromObjectToDto(List<User> users) {
        List<UserWithRankDto> list = new ArrayList<>();
        UserWithRankDto userWithRankDto;
        for (User user : users) {
            userWithRankDto = new UserWithRankDto();
            userWithRankDto.setId(user.getUserId());
            userWithRankDto.setUsername(user.getUserName());
            userWithRankDto.setRank(userScoreService.getByUserId(user.getUserId(), user).getCurrentRank());
            userWithRankDto.setScore(user.getUserScore().getTotalScore());
            list.add(userWithRankDto);
        }
        return list;
    }
}
