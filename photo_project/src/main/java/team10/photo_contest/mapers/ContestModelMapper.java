package team10.photo_contest.mapers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team10.photo_contest.dtos.ContestDto;
import team10.photo_contest.dtos.ContestUpdateDto;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Photo;
import team10.photo_contest.services.contracts.*;

import java.util.*;

import static team10.photo_contest.exceptions.ErrorMessages.COVER_PHOTO_EXISTS;

@Component
public class ContestModelMapper {
    private final ContestService contestService;
    private final PhotoService photoService;
    private final CategoryService categoryService;

    @Autowired
    public ContestModelMapper(ContestService contestService,
                              PhotoService photoService,
                              CategoryService categoryService) {
        this.contestService = contestService;
        this.photoService = photoService;
        this.categoryService = categoryService;

    }

    public Contest fromDto(ContestDto contestDto) {
        Contest contest = new Contest();
        dtoToObject(contest, contestDto);
        return contest;
    }

    public Contest addPhoto(Photo photo, int id) {
        Contest contest = contestService.getById(id);
        addPhotoToContest(contest, photo);
        return contest;
    }

    public List<String> createList(ContestUpdateDto dto) {
      return  Arrays.asList(dto.getUserNames().split(","));
    }

    private void addPhotoToContest(Contest contest, Photo photo) {
        if(contest.getPhoto() != null) throw new DuplicateEntityException(COVER_PHOTO_EXISTS);
        photo.setContestPhoto(true);
        photoService.update(photo);
        contest.setPhoto(photo);
    }

    private void dtoToObject(Contest contest, ContestDto contestDto) {
        contest.setTitle(contestDto.getTitle());
        contest.setCategories(categoryService.getByCategory(contestDto.getCategory()));
        if(contestDto.getIsOpen() == null) contestDto.setIsOpen("true");
        contest.setOpen(Boolean.parseBoolean(contestDto.getIsOpen()));
        contest.setPhaseITime(calculateDays(contestDto.getPhaseITime()));
        contest.setPhaseIITime(calculateHours(calculateDays(contestDto.getPhaseITime()), contestDto.getPhaseIITime()));
    }

    private Date calculateDays(int intDate) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, intDate);
        return calendar.getTime();
    }

    private Date calculateHours(Date date, int intHours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, intHours);
        return calendar.getTime();
    }
}
