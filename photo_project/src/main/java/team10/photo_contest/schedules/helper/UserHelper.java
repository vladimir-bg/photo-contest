package team10.photo_contest.schedules.helper;

public class UserHelper {
    private int userId;
    private int userScore;

    public UserHelper(int userId, int userScore) {
        this.userId = userId;
        this.userScore = userScore;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserScore() {
        return userScore;
    }

    public void setUserScore(int userScore) {
        this.userScore = userScore;
    }
}
