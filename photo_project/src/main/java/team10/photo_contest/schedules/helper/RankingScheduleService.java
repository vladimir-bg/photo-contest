package team10.photo_contest.schedules.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import team10.photo_contest.models.*;
import team10.photo_contest.repositories.contracts.UserRepository;
import team10.photo_contest.schedules.helper.UserHelper;
import team10.photo_contest.services.contracts.ContestService;
import team10.photo_contest.services.contracts.UserScoreService;

import java.util.*;

@Component
public class RankingScheduleService {
    private final ContestService contestService;
    private final UserScoreService userScoreService;
    private final UserRepository userRepository;

    @Autowired
    public RankingScheduleService(ContestService contestService, UserScoreService userScoreService, UserRepository userRepository) {
        this.contestService = contestService;
        this.userScoreService = userScoreService;
        this.userRepository = userRepository;
    }

    public void rankJob() {
        Date date = new Date();
        Set<Contest> contestSet = new HashSet<>(contestService.getAll(userRepository.getById(1)));
        for (Contest contest : contestSet) {
            if (contest.getPhaseITime().before(date)
                    && contest.getPhaseIITime().before(date)
                    && !contest.isFinished()
            ) {
                updateFromEntries(contest);
                updateContestToFinished(contest);
            }
        }
    }

    private void updateContestToFinished(Contest contest) {
        contest.setFinished(true);
        contestService.update(contest);
    }

    private void updateFromEntries(Contest contest) {
        Set<Entry> entrySet = contest.getContestEntries();
        if (!entrySet.isEmpty()) {
            Set<UserHelper> userHelperSet = new HashSet<>();
            for (Entry entry : entrySet) {
                Set<Review> reviewSet = entry.getReviews();
                int score = 0;
                if(entry.getReviews().size()==0){
                    if(contest.isOpen()) {
                        setUserScore(entry.getUser().getUserId(), 4);
                    }else{
                        setUserScore(entry.getUser().getUserId(), 6);
                    }
                    continue;
                }

                if (reviewSet.stream().allMatch(this::isEntryValid)) {
                    userHelperSet.add(new UserHelper(entry.getUser().getUserId(), 0));
                    continue;
                }

                for (Review review : reviewSet)
                    score += review.getScore();

                userHelperSet.add(new UserHelper(entry.getUser().getUserId(), score));
            }
            addParticipationPoints(userHelperSet,contest);
            makeRankingPositions(userHelperSet);
        }
    }

    private void addParticipationPoints(Set<UserHelper> userHelper, Contest contest){
        for (UserHelper user : userHelper) {
            if(contest.isOpen()){
                setUserScore(user.getUserId(),1);
            }else{
                setUserScore(user.getUserId(),3);
            }
        }
    }

    private boolean isEntryValid(Review review) {
        return review.getScore() == 0 && review.getComment().equals("The entry does not match the category!");
    }

    private void makeRankingPositions(Set<UserHelper> userHelper) {
        List<UserHelper> ranking = new ArrayList<>();
        int maxScoreFirst = 0;
        int maxScoreSecond = 0;
        int maxScoreThird = 0;

        for (UserHelper user : userHelper) {
            if (user.getUserScore() >= maxScoreFirst) {
                maxScoreFirst = score(ranking, user, 0, maxScoreFirst);
            } else if (user.getUserScore() >= maxScoreSecond) {
                maxScoreSecond = score(ranking, user, 1, maxScoreSecond);
            } else if (user.getUserScore() >= maxScoreThird) {
                maxScoreThird = score(ranking, user, 2, maxScoreThird);
            } else ranking.add(user);
        }
        updateUserScoreByPosition(ranking);
    }

    private int score(List<UserHelper> userList, UserHelper user, int top, int maxScore) {
        if (user.getUserScore() > maxScore) {
            maxScore = user.getUserScore();
            userList.add(top, user);
        } else {
            if (userList.size() > top) userList.add(top + 1, user);
            else userList.add(user);
        }
        return maxScore;
    }

    private void updateUserScoreByPosition(List<UserHelper> ranking) {
        int placeIncrement = 1;
        for (int i = 0; i < ranking.size(); i++) {
            switch (placeIncrement) {
                case 1:
                    if (ranking.get(0).getUserScore() >= ranking.get(1).getUserScore() * 2)
                        setUserScore(ranking.get(i).getUserId(), 75);
                    else i = givePointsByPlace(ranking, i, placeIncrement, 50, 40);
                    break;
                case 2:
                    i = givePointsByPlace(ranking, i, placeIncrement, 35, 25);
                    break;
                case 3:
                    i = givePointsByPlace(ranking, i, placeIncrement, 20, 10);
                    break;
            }
            placeIncrement++;
        }
    }

    private int givePointsByPlace(List<UserHelper> ranking, int i, int place, int points, int splitPoints) {
        int stop = 0;
        for (int j = i; j < ranking.size() - 1; j++) {
            if(ranking.get(j).getUserScore() == ranking.get(j + 1).getUserScore()){
                stop++;
            } else break;
        }

        if(stop == 0){
            setUserScore(ranking.get(i).getUserId(), points);
            return i;
        }

        for (int j = 0; j < stop+1; j++) {
            setUserScore(ranking.get(i + j).getUserId(), splitPoints);
        }
        return i + stop;
    }

    private void setUserScore(int userId, int userScoreInt) {
        User user = userRepository.getById(userId);
        UserScore userScore = user.getUserScore();
        userScore.setTotalScore(userScore.getTotalScore() + userScoreInt);
        user.setUserScore(userScore);
        userScoreService.update(user, userScore);
    }
}
