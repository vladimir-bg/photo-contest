package team10.photo_contest.exceptions;

public class NotAllowedOperationException extends RuntimeException{
    public NotAllowedOperationException(String message) {
        super(message);
    }
}
