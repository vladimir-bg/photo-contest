package team10.photo_contest.exceptions;

public class ContestIsNotInvitationalException extends RuntimeException{
    public ContestIsNotInvitationalException(String message) {
        super(message);
    }
}
