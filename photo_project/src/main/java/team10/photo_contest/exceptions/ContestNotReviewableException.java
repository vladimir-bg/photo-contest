package team10.photo_contest.exceptions;

public class ContestNotReviewableException extends RuntimeException {

    public ContestNotReviewableException(String message) {
        super(message);
    }
}
