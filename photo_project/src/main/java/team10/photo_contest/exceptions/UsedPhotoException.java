package team10.photo_contest.exceptions;

public class UsedPhotoException extends RuntimeException {
    public UsedPhotoException(String message) {
        super(String.format("%s can not be deleted",  message));
    }
}
