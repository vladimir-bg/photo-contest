package team10.photo_contest.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.User;
import team10.photo_contest.models.UserScore;
import team10.photo_contest.repositories.contracts.UserScoreRepository;

import java.util.List;

@Repository
public class UserScoreRepositoryImpl implements UserScoreRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public UserScoreRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getByRank(int min, int max) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(
                    "from User u where u.userScore.totalScore >= :min and u.userScore.totalScore <= :max ", User.class);
            query.setParameter("min", min);
            query.setParameter("max", max);

            List<User> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException(
                        String.format("users with score %d %d", min, max));
            }
            return result;
        }
    }

    @Override
    public void create(UserScore rankAndScore) {
        try (Session session = sessionFactory.openSession()) {
            session.save(rankAndScore);
        }
    }

    @Override
    public void update(UserScore rankAndScore) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(rankAndScore);
            session.getTransaction().commit();
        }
    }

    @Override
    public UserScore getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserScore rankAndScore = session.get(UserScore.class, id);
            if (rankAndScore == null) {
                throw new EntityNotFoundException("Score", id);
            }
            return rankAndScore;
        }
    }

    @Override
    public UserScore getByUserId(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserScore> query = session.createQuery(
                    "from UserScore where user.userId=:userId", UserScore.class);
            query.setParameter("userId", userId);

            List<UserScore> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException(
                        String.format("Rank or score for user with id: %d", userId));
            }
            return result.get(0);
        }
    }
}
