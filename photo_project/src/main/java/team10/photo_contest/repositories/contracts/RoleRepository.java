package team10.photo_contest.repositories.contracts;

import team10.photo_contest.models.Role;

import java.util.List;

public interface RoleRepository {

    List<Role> getAll();

    Role getByType(String type);
}
