package team10.photo_contest.repositories.contracts;

import team10.photo_contest.models.Category;

import java.util.List;

public interface CategoryRepository {
    Category getById(int id);
    Category getByCategory(String category);
    List<Category> getAll();
}
