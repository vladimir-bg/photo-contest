package team10.photo_contest.repositories.contracts;

import team10.photo_contest.models.User;

import java.util.List;

public interface UserRepository {

    void create(User user);

    void update(User user);

    void deactivate(int id);

    User getById(int id);

    User getByUsername(String username);

    List<User> getAllOrganizers();
    List<User> getAllAndSort();

    boolean isUserMaster(String username);

}
