package team10.photo_contest.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.ContestRepository;

import java.util.Date;
import java.util.List;

import static team10.photo_contest.exceptions.ErrorMessages.*;

@Repository
public class ContestRepositoryImpl implements ContestRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Contest getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Contest contest = session.get(Contest.class, id);
            if (contest == null) throw new EntityNotFoundException("contests", id);
            return contest;
        }
    }

    @Override
    public List<Contest> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest ", Contest.class);
            if (query.list().isEmpty()) throw new EntityNotFoundException("contests");
            return query.list();
        }
    }

    @Override
    public List<Entry> getEntriesByContestId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery("from Entry as e" +
                    " join fetch e.contest as ec " +
                    "where ec.contestId = :id", Entry.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_ENTRIES_IN_CONTEST);
            return query.list();
        }
    }

    @Override
    public Contest getByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where title=:title", Contest.class);
            query.setParameter("title", title);
            if (query.list().isEmpty()) throw new EntityNotFoundException("contests");
            return query.list().get(0);
        }
    }

    @Override
    public List<Contest> viewInPhase1() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest c " +
                    "where c.phaseITime > :date " +
                    "and c.phaseIITime > :date", Contest.class);
            query.setParameter("date", new Date());
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_CONTESTS_THIS_PHASE);
            return query.list();
        }
    }

    @Override
    public List<Contest> viewInPhase2() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest c " +
                    "where c.phaseITime < :date " +
                    "and c.phaseIITime > :date", Contest.class);
            query.setParameter("date", new Date());
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_CONTESTS_THIS_PHASE);
            return query.list();
        }
    }

    @Override
    public List<Contest> viewFinished() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(" from Contest c " +
                    " where c.phaseITime < :date " +
                    " and c.phaseIITime < :date ", Contest.class);
            query.setParameter("date", new Date());
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_CONTESTS_THIS_PHASE);
            return query.list();
        }
    }

    @Override
    public List<Contest> viewOpen() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest " +
                    "where isOpen = true ", Contest.class);
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_OPEN_CONTESTS_FOUND);
            return query.list();
        }
    }

    @Override
    public List<Contest> viewOpenInPhase1() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(" from Contest c " +
                    " where c.isOpen = true " +
                    " and c.phaseITime > :date " +
                    " and c.phaseIITime > :date ", Contest.class);
            query.setParameter("date", new Date());
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_OPEN_CONTESTS_THIS_PHASE);
            return query.list();
        }
    }

    @Override
    public List<Contest> viewInvitationalInPhase1(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(" from Contest c " +
                    " where :user in elements(c.invitedJunkies) " +
                    " and c.phaseITime > :date " +
                    " and c.phaseIITime > :date ", Contest.class);
            query.setParameter("date", new Date());
            query.setParameter("user", user);
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_INVITATIONAL_CONTESTS_THIS_PHASE);
            return query.list();
        }
    }

    @Override
    public List<Contest> finishedContestForUser(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(" select e.contest" +
                    " from Entry e " +
                    " left join e.user u " +
                    " left join e.contest c " +
                    " where u.id = :id " +
                    " and c.phaseITime < :date " +
                    " and c.phaseIITime < :date ", Contest.class);
            query.setParameter("date", new Date());
            query.setParameter("id", id);
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_SUCH_CONTEST_FOR_USER);
            return query.list();
        }
    }

    @Override
    public List<Contest> activeContestForUser(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery(" select e.contest" +
                    " from Entry e " +
                    " left join e.user u " +
                    " left join e.contest c " +
                    " where u.id = :id " +
                    " and c.phaseIITime > :date", Contest.class);
            query.setParameter("date", new Date());
            query.setParameter("id", id);
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_SUCH_CONTEST_FOR_USER);
            return query.list();
        }
    }

    @Override
    public List<Review> viewFinishedContestEntryReviews(int userId, int contestId) {
        try (Session session = sessionFactory.openSession()) {

            Query<Review> query = session.createQuery("from Review r " +
                    "join fetch r.entry re " +
                    "join fetch re.contest c " +
                    "where c.id=:contestId " +
                    "and re.user.id=:userId " +
                    "and c.phaseIITime < :date", Review.class);

            query.setParameter("date", new Date());
            query.setParameter("userId", userId);
            query.setParameter("contestId", contestId);
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_REVIEWS_FOUND);
            return query.list();
        }
    }

    @Override
    public List<Entry> viewAllFinishedContestEntryReviews(int contestId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Entry> query = session.createQuery("from Entry re " +
                    "join fetch re.contest c " +
                    "where c.id=:contestId " +
                    "and c.phaseIITime < :date", Entry.class);

            query.setParameter("date", new Date());
            query.setParameter("contestId", contestId);
            if (query.list().isEmpty()) throw new EntityNotFoundException(NO_REVIEWS_FOUND);
            return query.list();
        }
    }

    @Override
    public List<User> viewJury(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("select j " +
                    " from Contest c" +
                    " left join c.jury j " +
                    " where c.id = :id", User.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) throw new EntityNotFoundException("Jury in contest", id);
            return query.list();
        }
    }

    @Override
    public List<User> viewJunkies(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(" select u " +
                    " from Contest c" +
                    " left join c.contestEntries ce " +
                    " left join ce.user u " +
                    " where c.id = :id ", User.class);
            query.setParameter("id", id);
            if (query.list().isEmpty()) throw new EntityNotFoundException("Junkies in contest", id);
            return query.list();
        }
    }

    @Override
    public boolean isJurorInContest(int contestId, int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("select j " +
                    " from Contest c" +
                    " left join c.jury j " +
                    " where c.id = :contestId and j.id = :userId ", User.class);

            query.setParameter("contestId", contestId);
            query.setParameter("userId", userId);
            return !query.list().isEmpty();
        }
    }

    @Override
    public boolean isJunkieInContest(int contestId, int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("select j " +
                    " from Contest c" +
                    " left join c.invitedJunkies j " +
                    " where c.id = :contestId and j.id = :userId ", User.class);

            query.setParameter("contestId", contestId);
            query.setParameter("userId", userId);
            return !query.list().isEmpty();
        }
    }

    @Override
    public void create(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(contest);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(getById(id));
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(contest);
            session.getTransaction().commit();
        }
    }
}
