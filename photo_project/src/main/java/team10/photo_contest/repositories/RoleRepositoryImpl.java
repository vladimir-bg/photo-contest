package team10.photo_contest.repositories;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.Role;
import team10.photo_contest.repositories.contracts.RoleRepository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Role> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role", Role.class);
            return query.list();
        }
    }

    @Override
    public Role getByType(String type) {
        try (Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role where type=:type", Role.class);
            query.setParameter("type", type);
            List<Role> result = query.list();
            if (result.isEmpty()) {
                throw new EntityNotFoundException(
                        String.format("Role with type: %s not found", type));
            }
            return result.get(0);
        }
    }
}
