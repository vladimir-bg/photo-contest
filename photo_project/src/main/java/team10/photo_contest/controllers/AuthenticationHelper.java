package team10.photo_contest.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.NotAllowedOperationException;
import team10.photo_contest.exceptions.UnauthorizedOperationException;
import team10.photo_contest.models.User;
import team10.photo_contest.services.contracts.UserService;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String PASSWORD_HEADER_NAME = "Password";
    private final PasswordEncoder passwordEncoder;

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(PasswordEncoder passwordEncoder, UserService userService) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "You must log in to perform this action!");
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            String password = headers.getFirst(PASSWORD_HEADER_NAME);
            User user = userService.getByUsername(username);
            if (!passwordEncoder.matches(password,user.getPassword())) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username or password.");
            }
            return user;

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid username or password.");
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new UnauthorizedOperationException("No user logged in.");
        }
        try {
            return userService.getByUsername(currentUser);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException("No user logged in.");
        }
    }

    public User verifyAuthentication(String userName, String password) {
        try {
            User user = userService.getByUsername(userName);
            if(!user.isActive()){
                throw new NotAllowedOperationException("Account is no longer active!");
            }

            if (!passwordEncoder.matches(password,user.getPassword())) {
                throw new UnauthorizedOperationException("Invalid username or password.");
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException("Invalid username or password.");
        }
    }

}
