package team10.photo_contest.controllers.rest;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.dtos.EntryDto;
import team10.photo_contest.dtos.PhotoFromUrlDto;
import team10.photo_contest.mapers.EntryModelMapper;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.User;
import team10.photo_contest.services.contracts.EntryService;
import team10.photo_contest.services.contracts.PhotoService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/entry")
public class EntryController {
    private final EntryService entryService;
    private final EntryModelMapper entryModelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final PhotoService photoService;

    @Autowired
    public EntryController(EntryService entryService,
                           EntryModelMapper entryModelMapper,
                           AuthenticationHelper authenticationHelper,
                           PhotoService photoService) {
        this.entryService = entryService;
        this.entryModelMapper = entryModelMapper;
        this.authenticationHelper = authenticationHelper;
        this.photoService = photoService;
    }

    @GetMapping
    @ApiOperation(value = "Find all available entries in the system",
            notes = "Need authorization",
            response = Entry.class)
    public List<Entry> getAll(@RequestHeader HttpHeaders headers) {
        authenticationHelper.tryGetUser(headers);
        return entryService.getAll();
    }

    @GetMapping("/id/{id}")
    @ApiOperation(value = "Find entry by id",
            notes = "Need authorization and id",
            response = Entry.class)
    public Entry getById(@RequestHeader HttpHeaders headers, @Valid @PathVariable int id) {
        authenticationHelper.tryGetUser(headers);
        return entryService.getById(id);
    }

    @PostMapping
    @ApiOperation(value = "Create entry",
            notes = "Need authorization and EntryDto",
            response = Entry.class)
    public Entry create(@RequestHeader HttpHeaders headers, @Valid @RequestBody EntryDto entryDto) {
        authenticationHelper.tryGetUser(headers);
        Entry entry = entryModelMapper.fromDto(entryDto);
        entryService.create(entry);
        return entry;
    }

    @GetMapping("/users/{id}")
    @ApiOperation(value = "Find entries by user id",
            notes = "Need authorization and user id",
            response = Entry.class)
    public List<Entry> getByUserId(@RequestHeader HttpHeaders headers, @Valid @PathVariable int id) {
        authenticationHelper.tryGetUser(headers);
        return entryService.getByUserId(id);
    }

    @GetMapping("/contests-id/{id}")
    @ApiOperation(value = "Find entries by contest id",
            notes = "Need authorization and contest id",
            response = Entry.class)
    public List<Entry> viewParticipatedContestEntries(@RequestHeader HttpHeaders headers, @Valid @PathVariable int id) {
        authenticationHelper.tryGetUser(headers);
        return entryService.getByUserId(id);
    }

    @PostMapping("/{id}/upload/photo")
    @ApiOperation(value = "Upload photo to entry",
            notes = "Need authorization, id for entry and photo file",
            response = Entry.class)
    public Entry addPhoto(@RequestHeader HttpHeaders headers,
                   @RequestParam("file") MultipartFile file,
                   @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        Entry entry = entryModelMapper.addPhoto(photoService.storeFile(file, user), id);
        entryService.update(entry, user);
        return entry;
    }

    @PostMapping("/{id}/upload/photo/from-url")
    @ApiOperation(value = "Upload photo to entry",
            notes = "Need authorization, id for entry and photo url",
            response = Entry.class)
    public Entry addPhotoUrl(@RequestHeader HttpHeaders headers,
                      @Valid @RequestBody PhotoFromUrlDto photoFromUrlDto,
                      @PathVariable int id) {
        User user = authenticationHelper.tryGetUser(headers);
        Entry entry = entryModelMapper.addPhoto(photoService.storeFromUrl(photoFromUrlDto, user), id);
        entryService.update(entry, user);
        return entry;
    }
}
