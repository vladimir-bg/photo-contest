package team10.photo_contest.controllers.rest;

import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.dtos.PhotoFromUrlDto;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Photo;
import team10.photo_contest.models.User;
import team10.photo_contest.services.contracts.PhotoService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;


@RestController
@RequestMapping("/api/v1/photos")
public class PhotoController {
    private static final Logger logger = LoggerFactory.getLogger(PhotoController.class);
    private final PhotoService photoService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PhotoController(PhotoService photoService, AuthenticationHelper authenticationHelper) {
        this.photoService = photoService;
        this.authenticationHelper = authenticationHelper;
    }

    @PostMapping("/upload")
    @ApiOperation(value = "Upload photo to the system",
            notes = "Need authorization",
            response = Photo.class)
    public Photo uploadPhotoToArchive(@RequestHeader HttpHeaders headers, @RequestParam("file") MultipartFile file) {
        User user = authenticationHelper.tryGetUser(headers);
        return photoService.storeFile(file, user);
    }

    @PostMapping("/upload/from-url")
    @ApiOperation(value = "Upload photo from url  to the system",
            notes = "Need authorization",
            response = Photo.class)
    public Photo getImageAsResource(@RequestHeader HttpHeaders headers, @Valid @RequestBody PhotoFromUrlDto dto) {
        User user = authenticationHelper.tryGetUser(headers);
        return photoService.storeFromUrl(dto, user);
    }

    @GetMapping("/{id}/view")
    @ApiOperation(value = "Get photo by id from system",
            notes = "Need authorization and id",
            response = Photo.class)
    public ResponseEntity<Resource> viewSinglePhoto(@RequestHeader HttpHeaders headers,
                                                  @PathVariable int id, HttpServletRequest request) {
        User user = authenticationHelper.tryGetUser(headers);
        Resource resource = photoService.viewPhoto(id, user);

        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                        + resource.getFilename() + "\"")
                .body(resource);
    }
}
