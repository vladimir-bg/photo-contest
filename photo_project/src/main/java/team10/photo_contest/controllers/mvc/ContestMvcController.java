package team10.photo_contest.controllers.mvc;

import org.springframework.beans.NotReadablePropertyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.controllers.utils.FileModel;
import team10.photo_contest.dtos.ContestDto;
import team10.photo_contest.dtos.ContestUpdateDto;
import team10.photo_contest.dtos.EntryDto;
import team10.photo_contest.exceptions.*;
import team10.photo_contest.mapers.ContestModelMapper;
import team10.photo_contest.mapers.UserScoreModelMapper;
import team10.photo_contest.models.Contest;
import team10.photo_contest.models.Photo;
import team10.photo_contest.models.User;
import team10.photo_contest.services.contracts.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

import static team10.photo_contest.exceptions.ErrorMessages.PHOTO_MISSING;
import static team10.photo_contest.exceptions.ErrorMessages.VALID_PHOTO_NAME;

@Controller
@RequestMapping("/contests")
public class ContestMvcController {
    private final ContestService contestService;
    private final ContestModelMapper contestModelMapper;
    private final CategoryService categoryService;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserScoreService userScoreService;
    private final UserScoreModelMapper userScoreModelMapper;
    private final PhotoService photoService;

    @Autowired
    public ContestMvcController(ContestService contestService,
                                ContestModelMapper contestModelMapper,
                                CategoryService categoryService,
                                AuthenticationHelper authenticationHelper,
                                UserService userService,
                                UserScoreService userScoreService,
                                UserScoreModelMapper userScoreModelMapper,
                                PhotoService photoService) {
        this.contestService = contestService;
        this.contestModelMapper = contestModelMapper;
        this.categoryService = categoryService;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userScoreService = userScoreService;
        this.userScoreModelMapper = userScoreModelMapper;
        this.photoService = photoService;
    }

    @GetMapping("/create")
    public String getContestDto(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer()) return "redirect:/";
            model.addAttribute("contestDto", new ContestDto());
            model.addAttribute("allCategories", categoryService.getAll());
            model.addAttribute("fileToUpload", new FileModel());
            try{
                model.addAttribute("allUserPhotos", photoService.getByUserId(user.getUserId()));
            } catch (EntityNotFoundException e) {
                model.addAttribute("allUserPhotos", null);
            }
            return "createContest";
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        }
    }

    @PostMapping("/create")
    public String createContest(@Valid @ModelAttribute("contestDto") ContestDto contestDto,
                                BindingResult errors,
                                @ModelAttribute("fileToUpload") FileModel fileModel,
                                Model model, HttpSession session) {
        if (errors.hasErrors()) {
            model.addAttribute("allCategories", categoryService.getAll());
            model.addAttribute("fileToUpload", new FileModel());
            return "createContest";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer()) return "redirect:/";
            Contest contest = contestModelMapper.fromDto(contestDto);

            EntryDto entryDto=new EntryDto();
            entryDto.setPhotoName(contestDto.getPhotoName());
            entryDto.setPhotoUrl(contestDto.getPhotoUrl());
            Photo photo = photoService.saveOrGetContestPhoto(fileModel, entryDto, user);


            photo.setContestPhoto(true);
            photoService.update(photo);
            contest.setPhoto(photo);
            contestService.create(contest, user);
            if (contest.isOpen()) {
                model.addAttribute("juryDto", new ContestUpdateDto());
                model.addAttribute("contestId", contest.getContestId());
                try{
                    model.addAttribute("juryList", userScoreModelMapper.fromObjectToDto(
                            userScoreService.getByRank(151, 1000)));
                } catch (EntityNotFoundException e) {
                    return "redirect:/organizer";
                }
                return "contestAddJury";
            } else {
                model.addAttribute("junkieDto", new ContestUpdateDto());
                model.addAttribute("contestId", contest.getContestId());
                model.addAttribute("junkieList", userScoreModelMapper.fromObjectToDto(
                        userService.getAllAndSort(user)));
                return "contestAddJunkie";
            }
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        } catch(DuplicateEntityException e) {
            errors.rejectValue("title", "contest", e.getMessage());
            model.addAttribute("duplicateName", e.getMessage());
            return "createContest";
        } catch(EntityNotFoundException e) {
            errors.rejectValue("photoName", "contest", PHOTO_MISSING);
            return "createContest";
        }catch (FileStorageException | NotReadablePropertyException e) {
            errors.rejectValue("photoUrl", "wrong_input",VALID_PHOTO_NAME);
            return "createContest";
        }

    }

    @PostMapping("/{id}/add/junkies")
    public String addJunkieToContest(@PathVariable int id,
                                     @Valid @ModelAttribute("junkieDto") ContestUpdateDto contestUpdateDto,
                                     BindingResult errors,
                                     Model model, HttpSession session) {
        if (errors.hasErrors()) {
            return "contestAddJunkie";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer()) return "redirect:/";
            List<String> junkies = contestModelMapper.createList(contestUpdateDto);
            contestService.updateJunkies(id, junkies, user);
            model.addAttribute("contestId", id);
            model.addAttribute("juryDto", new ContestUpdateDto());
            model.addAttribute("juryList", userScoreModelMapper.fromObjectToDto(
                    userScoreService.getByRank(151, 1000)));
            return "contestAddJury";
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        } catch (NotAllowedOperationException e) {
            errors.rejectValue("userNames", "junkie", e.getMessage());
            return "contestAddJunkie";
        }
    }

    @PostMapping("/{id}/add/jury")
    public String addJuryToContest(@PathVariable int id,
                                   @ModelAttribute("juryDto") ContestUpdateDto contestUpdateDto,
                                   BindingResult errors, HttpSession session) {
        if (errors.hasErrors()) {
            return "contestAddJury";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer()) return "redirect:/";
            if(contestUpdateDto == null) return "redirect:/contests/create";
            List<String> jury = contestModelMapper.createList(contestUpdateDto);
            contestService.updateJury(id, jury, user);
            return "redirect:/organizer";
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        } catch(NotAllowedOperationException e) {
            errors.rejectValue("userNames", "jury", e.getMessage());
            return "contestAddJury";
        }
    }

    @GetMapping("/phase/1")
    public String getContestsInPhase1(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer()) return "redirect:/";
            model.addAttribute("contestList", contestService.viewInPhase1(user));
            return "contestShow";
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("contestError", "contestError");
            return "contestShow";
        }
    }

    @GetMapping("/phase/2")
    public String getContestsInPhase2(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer()) return "redirect:/";
            model.addAttribute("contestList", contestService.viewInPhase2(user));
            return "contestShow";
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("contestError", "contestError");
            return "contestShow";
        }
    }

    @GetMapping("/finished")
    public String getContestsFinished(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer()) return "redirect:/";
            model.addAttribute("contestList", contestService.viewFinished(user));
            return "contestShow";
        } catch(UnauthorizedOperationException e) {
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("contestError", "contestError");
            return "contestShow";
        }
    }

    @GetMapping("/{id}/all-entries")
    public String viewUserAllEntryFinishedContestReviews(HttpSession session, Model model,
                                                         @Valid @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            if(!user.isOrganizer() && user.getUserScore().getTotalScore() <= 150) return "redirect:/";
            model.addAttribute("contestId", id);
            model.addAttribute("contestEntries", contestService.getEntriesByContestId(id));
            model.addAttribute("contestIsInPhase2", contestService.isContestPhase2(id));
            return "contestAllEntries";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "contestAllEntries";
        }
    }
}
