package team10.photo_contest.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.dtos.LoginDto;
import team10.photo_contest.dtos.UserRegisterDto;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.NotAllowedOperationException;
import team10.photo_contest.exceptions.UnauthorizedOperationException;
import team10.photo_contest.mapers.UserModelMapper;
import team10.photo_contest.models.User;
import team10.photo_contest.models.UserScore;
import team10.photo_contest.services.contracts.UserScoreService;
import team10.photo_contest.services.contracts.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {
    private final UserService userService;
    private final UserModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;
    private final UserScoreService userScoreService;

    @Autowired
    public AuthenticationMvcController(UserService userService,
                                       UserModelMapper modelMapper,
                                       AuthenticationHelper authenticationHelper,
                                       UserScoreService userScoreService) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
        this.userScoreService = userScoreService;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session,Model model) {
        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            User user = authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            if (user.isOrganizer()) {
                return "redirect:/organizer";
            } else
                return "redirect:/users";
        } catch (UnauthorizedOperationException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }catch (NotAllowedOperationException e){
            model.addAttribute("inactive", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserRegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String createUser(@Valid @ModelAttribute("user") UserRegisterDto registerDto,
                             BindingResult errors) {
        if (errors.hasErrors()) {
            return "register";
        }

        if (!registerDto.getPassword().equals(registerDto.getRepeatPassword())) {
            errors.rejectValue("password", "password_error",
                    "Password confirmation should match password.");
            return "register";
        }

        try {
            User newUser = modelMapper.fromDto(registerDto, new User());
            User user = userService.create(newUser);
            user.setUserScore(userScoreService.create(user, new UserScore()));
            userService.update(user, user);
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("username", "duplicate_username", e.getMessage());
            return "register";
        }
    }
}
