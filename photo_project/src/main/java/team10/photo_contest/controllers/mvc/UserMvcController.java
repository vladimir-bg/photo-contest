package team10.photo_contest.controllers.mvc;

import org.springframework.beans.NotReadablePropertyException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.controllers.utils.FileModel;
import team10.photo_contest.dtos.EntryDto;
import team10.photo_contest.dtos.PhotoFromUrlDto;
import team10.photo_contest.dtos.UserEditDto;
import team10.photo_contest.dtos.output.EntryViewDto;
import team10.photo_contest.dtos.output.EntryWithReviewsDto;
import team10.photo_contest.dtos.output.RankDto;
import team10.photo_contest.exceptions.*;
import team10.photo_contest.mapers.EntryModelMapper;
import team10.photo_contest.mapers.UserModelMapper;
import team10.photo_contest.models.*;
import team10.photo_contest.services.contracts.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static team10.photo_contest.exceptions.ErrorMessages.NO_AVAILABLE_CONTESTS;
import static team10.photo_contest.exceptions.ErrorMessages.VALID_PHOTO_NAME;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    private final PhotoService photoService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final EntryService entryService;
    private final EntryModelMapper entryModelMapper;
    private final UserScoreService scoreService;
    private final UserModelMapper userModelMapper;
    private final UserService userService;


    @Autowired
    public UserMvcController(PhotoService photoService,
                             AuthenticationHelper authenticationHelper,
                             ContestService contestService,
                             EntryService entryService,
                             EntryModelMapper entryModelMapper,
                             UserScoreService scoreService,
                             UserModelMapper userModelMapper,
                             UserService userService) {
        this.photoService = photoService;
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.entryService = entryService;
        this.entryModelMapper = entryModelMapper;
        this.scoreService = scoreService;
        this.userModelMapper = userModelMapper;
        this.userService = userService;
    }

    @GetMapping
    public String showUsersPage(Model model, HttpSession session) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("score", scoreService.getByUserId(currentUser.getUserId(), currentUser));
            return "userHomePage";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/photo-archive/upload")
    public String addPhotoToArchive(Model model) {

        PhotoFromUrlDto addPhotoDto = new PhotoFromUrlDto();
        FileModel photoFile = new FileModel();
        model.addAttribute("fileToUpload", photoFile);
        model.addAttribute("fileFromUrl", addPhotoDto);
        return "photoEntry";

    }

    @PostMapping("/photo-archive/upload")
    public String addPhotoToArchive(HttpSession session,
                                    @ModelAttribute("fileToUpload") FileModel file,
                                    @ModelAttribute("fileFromUrl") PhotoFromUrlDto dto,
                                    BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "photoEntry";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            photoService.saveOrGetContestPhoto(file, entryModelMapper.convertToEntryDto(dto), user);
            return "redirect:/users";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (FileStorageException | NotReadablePropertyException e) {
            result.rejectValue("photoUrl", "wrong_input", VALID_PHOTO_NAME);
            return "photoEntry";
        } catch (DuplicateEntityException e) {
            model.addAttribute("duplicateName", e.getMessage());
            return "photoEntry";
        }
    }

    @GetMapping("/contests/entered/finished")
    public String getFinishedParticipatedContests(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> enteredFinishedContests = contestService.finishedContestForUser(user.getUserId());
            model.addAttribute("finishedEnteredContests", enteredFinishedContests);
            return "enteredFinishedContests";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "enteredFinishedContests";
        }
    }

    @GetMapping("/contests/entered/active")
    public String getActiveParticipatedContests(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> enteredActiveContests = contestService.activeContestForUser(user.getUserId());
            model.addAttribute("activeEnteredContests", enteredActiveContests);
            return "enteredActiveContests";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "enteredActiveContests";
        }
    }

    @GetMapping("/contests/{id}/all-entries")
    public String viewUserAllEntryFinishedContestReviews(HttpSession session, Model model,
                                                         @Valid @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<EntryWithReviewsDto> listEntries = contestService.viewAllFinishedContestEntryReviews(user, id);
            model.addAttribute("finishedContestEntries", listEntries);
            return "finishedContestEntries";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "finishedContestEntries";
        }
    }

    @GetMapping("/contests/{id}/my-entry")
    public String viewMyContestEntry(HttpSession session, Model model,
                                     @Valid @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            EntryViewDto myEntry = entryService.viewMyContestEntry(user, id);
            model.addAttribute("myContestEntry", myEntry);
            model.addAttribute("entryScore", entryService.viewFinishedContestEntryScore(myEntry.getEntryId()));
            model.addAttribute("currentDate", new Date());
            return "viewSingleEntry";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "viewSingleEntry";
        }
    }

    @GetMapping("/contests/{id}/entry/{id}")
    public String viewContestEntry(HttpSession session, Model model,
                                   @Valid @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Entry entry = entryService.getById(id);
            model.addAttribute("contestEntry", entry);
            model.addAttribute("entryScore", entryService.viewFinishedContestEntryScore(entry.getEntryId()));
            return "viewSingleEntry";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "viewSingleEntry";
        }
    }

    @GetMapping("/photo/{id}/view")
    public String viewSinglePhoto(HttpSession session, Model model, @PathVariable int id, HttpServletRequest request) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Resource resource = photoService.viewPhoto(id, user);
            String photoToView = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            String photoToView2 = resource.getFile().getName();
            model.addAttribute("photoToView", photoToView);
            return photoToView2;
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException | IOException e) {
            model.addAttribute("error", e.getMessage());
            return "404-notFound";
        }
    }

    @GetMapping("/user/score")
    public String viewRankAndScore(HttpSession session, Model model, HttpServletRequest request) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            RankDto userscore = scoreService.getByUserId(user.getUserId(), user);
            model.addAttribute("userScore", userscore);
            return "viewRankAndScore";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404-notFound";
        }
    }

    @GetMapping("/contests/open")
    public String getOpenContestsInPhase1(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> openToParticipateContests = contestService.viewOpenInPhase1();
            model.addAttribute("openToParticipateContests",
                    contestService.contestsNotEnteredByUser(openToParticipateContests, user));
            return "openToParticipateContests";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", NO_AVAILABLE_CONTESTS);
            return "openToParticipateContests";
        }
    }

    @GetMapping("/contests/invitational")
    public String getClosedContestsInPhase1(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<Contest> openToParticipateContests = contestService.viewInvitationalInPhase1(user);
            model.addAttribute("invitedToParticipateContests",
                    contestService.contestsNotEnteredByUser(openToParticipateContests, user));
            return "invitedToParticipateContests";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", NO_AVAILABLE_CONTESTS);
            return "invitedToParticipateContests";
        }
    }

    @GetMapping("/contests/{id}/enter")
    public String createContestEntry(HttpSession session, Model model, @PathVariable int id) {

        try {
            User user = authenticationHelper.tryGetUser(session);
            EntryDto entryDto = new EntryDto();
            entryDto.setContestId(id);
            entryDto.setUserId(user.getUserId());
            FileModel photoFile = new FileModel();
            model.addAttribute("fileToUpload", photoFile);
            model.addAttribute("entryDto", entryDto);
            model.addAttribute("allMyPhotos", photoService.getByUserId(user.getUserId()));
            return "createContestEntry";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("allMyPhotos", null);
            return "createContestEntry";
        }
    }

    @PostMapping("/contests/{id}/enter")
    public String createContestEntry(HttpSession session, @ModelAttribute("fileToUpload") FileModel file,
                                     @ModelAttribute("entryDto") EntryDto dto,
                                     BindingResult result, @PathVariable int id, Model model) {
        if (result.hasErrors()) {
            return "userHomePage";
        }
        try {
            User user = authenticationHelper.tryGetUser(session);
            dto.setContestId(id);
            dto.setUserId(user.getUserId());
            Entry entry = entryModelMapper.fromDto(dto);
            Photo photo = photoService.saveOrGetContestPhoto(file, dto, user);
            photo.setContestPhoto(true);
            entry.setPhoto(photo);
            entryService.create(entry);
            photoService.update(photo);

            return "redirect:/users";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (DuplicateEntityException e) {
            model.addAttribute("duplicateName", e.getMessage());
            return "createContestEntry";
        } catch (FileStorageException | NotReadablePropertyException e) {
            result.rejectValue("photoUrl", "wrong_input", VALID_PHOTO_NAME);
            return "createContestEntry";
        } catch (NotAllowedOperationException e) {
            model.addAttribute("userIsJury", e.getMessage());
            return "createContestEntry";
        }
    }

    @GetMapping("/photo-archive/view-all")
    public String showAllMyPhotos(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            photoService.getByUserId(user.getUserId());
            model.addAttribute("allPhotos", photoService.getByUserId(user.getUserId()));
            return "viewAllUserPhotos";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "viewAllUserPhotos";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
    }


    @GetMapping("/entries/{id}/all-reviews")
    public String showAllEntryReviews(HttpSession session, Model model, @PathVariable int id) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            List<Review> entryReviews = entryService.viewMyContestEntryReviews(id, user);
            model.addAttribute("entryReviews", entryReviews);
            return "viewEntryReviews";
        } catch (EntityNotFoundException | NotAllowedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "viewEntryReviews";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/contests/reviewable")
    public String getAllReviewableContests(HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("InvitedContestsToReview",
                    contestService.viewContestsWhenJury(user));
            return "invitedToReviewContests";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", NO_AVAILABLE_CONTESTS);
            return "invitedToReviewContests";
        }
    }

    @GetMapping("/account/edit")
    public String showEditPage(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            model.addAttribute("userEdit", userModelMapper.toDto(user, new UserEditDto()));
            model.addAttribute("user", user);
            return "userEditPage";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

    }

    @PostMapping("/account/edit")
    public String editUser(@Valid @ModelAttribute("userEdit") UserEditDto userEditDto,
                           BindingResult errors, HttpSession session, Model model) {
        if (errors.hasErrors()) {
            return "userEditPage";
        }
        if (!userEditDto.getPassword().equals(userEditDto.getRepeatPassword())) {
            errors.rejectValue("password", "password_error",
                    "Password confirmation should match password.");
            return "userEditPage";
        }

        try {
            User user = authenticationHelper.tryGetUser(session);
            User userToUpdate = userModelMapper.fromDto(userEditDto, user.getUserId());
            userService.update(userToUpdate, user);
            model.addAttribute("user", user);
            return "redirect:/users";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "incorrect_name", e.getMessage());
            return "userEditPage";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/{id}/deactivate")
    public String deactivate(HttpSession session, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            userService.deactivate(id, user);
            session.removeAttribute("currentUser");
            return "redirect:/";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
    }
}
