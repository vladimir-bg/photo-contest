package team10.photo_contest.controllers.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import team10.photo_contest.controllers.AuthenticationHelper;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.UnauthorizedOperationException;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.User;
import team10.photo_contest.services.contracts.ContestService;
import team10.photo_contest.services.contracts.UserService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final UserService userService;

    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper, ContestService contestService, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.userService = userService;
    }

    @ModelAttribute("currentUser")
    public User addCurrentUser(HttpSession session) {
        try {
            return authenticationHelper.tryGetUser(session);
        } catch (UnauthorizedOperationException ignore) {}
        return null;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("currentUser", null);
        }

        try {
            List<Entry> winningEntry = contestService.viewLastThreeWinningEntries();
            model.addAttribute("winningEntries", winningEntry);
        }catch (EntityNotFoundException e){
            model.addAttribute("error", e.getMessage());
            return "index";
        }
        return "index";
    }

    @GetMapping("/about")
    public String aboutPhotoContest(Model model) {
        model.addAttribute("numberOfUsers", userService.getTotalUsers());

        return "aboutPhotoContest";

    }

    @GetMapping("/rules")
    public String contestRules() {
        return "theRules";

    }
}
