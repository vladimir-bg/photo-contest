package team10.photo_contest.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
@ConfigurationProperties(prefix = "file")
@PropertySource("classpath:application.properties")
public class FileStorageProperties {
    private String uploadDir;

    public String getUploadDir(){
        return uploadDir;
    }
    @Bean
    public Path getFileStorageLocation(){
        return Paths.get(getUploadDir())
                .toAbsolutePath().normalize();
    }

    public void setUploadDir(String uploadDir){
        this.uploadDir=uploadDir;
    }
}
