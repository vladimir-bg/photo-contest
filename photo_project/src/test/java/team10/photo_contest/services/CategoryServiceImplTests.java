package team10.photo_contest.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.Category;
import team10.photo_contest.repositories.contracts.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

import static team10.photo_contest.Helpers.createMockCategory;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    CategoryRepository categoryRepository;

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Test
    public void getById_Should_ReturnCategory_When_MatchExists() {
        //Arrange
        Mockito.when(categoryRepository.getById(1))
                .thenReturn(createMockCategory());

        //Act
        Category result = categoryService.getById(1);

        //Assert
        Assertions.assertEquals(1, result.getCategoryId());
        Assertions.assertEquals("Puppies", result.getCategoryType());
    }

    @Test
    public void getByCategory_Should_ReturnCategory_When_MatchExists() {
        //Arrange
        Mockito.when(categoryRepository.getByCategory("Puppies"))
                .thenReturn(createMockCategory());

        //Act
        Category result = categoryService.getByCategory("Puppies");

        //Assert
        Assertions.assertEquals(1, result.getCategoryId());
        Assertions.assertEquals("Puppies", result.getCategoryType());
    }

    @Test
    public void getAll_Should_ReturnAllCategories(){
        //Arrange
        var categoryOne=createMockCategory();

        var categoryTwo=createMockCategory();
        categoryTwo.setCategoryId(2);
        categoryTwo.setCategoryType("Dolphins");

        List<Category> list=new ArrayList<>();
        list.add(categoryOne);
        list.add(categoryTwo);
        Mockito.when(categoryRepository.getAll())
                .thenReturn(list);

        //Act
        List<Category> result=categoryService.getAll();

        //Assert
        Assertions.assertTrue(result.contains(categoryOne));
        Assertions.assertTrue(result.contains(categoryTwo));
    }

    @Test
    public void getById_Should_Throw_When_NoMachExist(){
        //Arrange
        Mockito.when(categoryRepository.getById(1)).thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> categoryService.getById(1));
    }

    @Test
    public void getByCategory_Should_Throw_When_NoMachExist(){
        //Arrange
        Mockito.when(categoryRepository.getByCategory("Cats")).thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> categoryService.getByCategory("Cats"));
    }
}
