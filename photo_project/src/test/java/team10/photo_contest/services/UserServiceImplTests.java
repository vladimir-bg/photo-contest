package team10.photo_contest.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import team10.photo_contest.Helpers;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.UserRepository;
import team10.photo_contest.services.utils.VerificationHelper;

import java.util.ArrayList;
import java.util.List;

import static team10.photo_contest.Helpers.createMockScore;
import static team10.photo_contest.Helpers.createMockUser1;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {
    @Mock
    UserRepository userRepository;
    @Mock
    VerificationHelper verificationHelper;
    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void create_Should_ThrowException_When_UserDuplicateExists() {
        // Arrange
        var mockUser1 = Helpers.createMockUser1();
        var mockUser2 = Helpers.createMockUser1();
        mockUser2.setUserId(3);

        Mockito.when(userRepository.getByUsername(mockUser1.getUserName())).thenReturn(mockUser2);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(mockUser1));
    }

    @Test
    public void create_Should_CallRepository_When_UserDuplicateDoesNotExist() {
        // Arrange
        var mockUser1 = Helpers.createMockUser1();
        Mockito.when(userRepository.getByUsername(mockUser1.getUserName())).thenReturn(mockUser1);

        //Act
        userService.create(mockUser1);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .create(Mockito.any(User.class));
    }
    @Test
    public void create_Should_CallRepository_When_UserDuplicateDoesNotExist2() {
        // Arrange
        var mockUser1 = Helpers.createMockUser1();
        Mockito.when(userRepository.getByUsername(mockUser1.getUserName())).thenThrow(EntityNotFoundException.class);

        //Act
        userService.create(mockUser1);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .create(Mockito.any(User.class));
    }

    @Test
    public void update_Should_ThrowException_When_UserToUpdateNotFound() {
        // Arrange
        var mockUser1 = Helpers.createMockUser1();
        var mockUser2 = Helpers.createMockUser1();
        mockUser2.setUserId(3);

        Mockito.when(userRepository.getByUsername(mockUser1.getUserName())).thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> userService.update(mockUser1,mockUser2));
    }
    @Test
    public void update_Should_ThrowException_When_DuplicateUserExists() {
        // Arrange
        var mockUser1 = Helpers.createMockUser1();
        var mockUser2 = Helpers.createMockUser1();
        mockUser2.setUserId(4);


        Mockito.when(userRepository.getByUsername(mockUser1.getUserName())).thenReturn(mockUser2);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.update(mockUser1,mockUser2));
    }

    @Test
    public void update_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockUser1 = Helpers.createMockUser1();
        var mockUser2 = Helpers.createMockUser1();

        Mockito.when(userRepository.getByUsername(mockUser1.getUserName())).thenReturn(mockUser1);
        //Act
        userService.update(mockUser1,mockUser2);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .update(Mockito.any(User.class));
    }

    @Test
    public void deactivate_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockUser1 = Helpers.createMockUser1();
        var mockUser2 = Helpers.createMockUser1();

        //Act
        userService.deactivate(mockUser1.getUserId(),mockUser2);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .deactivate(Mockito.anyInt());
    }

    @Test
    public void getById_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockUser1 = Helpers.createMockUser1();
        var mockUser2 = Helpers.createMockUser1();

        //Act
        userService.getById(mockUser1.getUserId(),mockUser2);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .getById(Mockito.anyInt());
    }

    @Test
    public void getById_Should_ReturnUser_When_MatchExists() {
        //Arrange
        var mockCaller=Helpers.createMockUser2();
        Mockito.when(userRepository.getById(Mockito.anyInt()))
                .thenReturn(createMockUser1());

        //Act
        User result = userService.getById(1,mockCaller);

        //Assert
        Assertions.assertEquals(1, result.getUserId());
        Assertions.assertEquals("jorko1", result.getUserName());
    }

    @Test
    public void getAllAndSort_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockCaller = Helpers.createMockUser2();

        //Act
        userService.getAllAndSort(mockCaller);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .getAllAndSort();
    }

    @Test
    public void getAllAndSort_Should_ReturnSortedUserList_When_CallerIsVerified() {
        //Arrange
        var mockUser1=Helpers.createMockUser1();
        var mockUser2=Helpers.createMockUser1();
        var mockCaller=Helpers.createMockUser2();
        var mockScore1=createMockScore();
        var mockScore2=createMockScore();
        mockUser2.setUserId(3);
        mockScore2.setUser(mockUser2);
        mockScore2.setTotalScore(50);
        mockScore1.setTotalScore(20);
        mockUser1.setUserScore(mockScore1);
        mockUser2.setUserScore(mockScore2);
        List<User> list=new ArrayList<>();
        list.add(mockUser1);
        list.add(mockUser2);

        Mockito.when(userRepository.getAllAndSort())
                .thenReturn(list);

        //Act
        List<User> result = userService.getAllAndSort(mockCaller);

        //Assert
        Assertions.assertEquals(1, result.get(0).getUserId());
        Assertions.assertEquals(3, result.get(1).getUserId());
        Assertions.assertEquals(20, result.get(0).getUserScore().getTotalScore());
        Assertions.assertEquals(50, result.get(1).getUserScore().getTotalScore());
    }

    @Test
    public void geByUsername_Should_ReturnUserName_When_NothingIsThrown() {
        //Arrange
        Mockito.when(userRepository.getByUsername("jorko1"))
                .thenReturn(createMockUser1());

        //Act
        User result = userService.getByUsername("jorko1");

        //Assert
        Assertions.assertEquals(1, result.getUserId());
        Assertions.assertEquals("jorko1", result.getUserName());
    }

    @Test
    public void geByUsername_Should_CallRepository_When_NothingIsThrown() {

        //Arrange, Act
        userService.getByUsername("jorko1");

        //Assert
        Mockito.verify(userRepository, Mockito.times(1))
                .getByUsername("jorko1");
    }




}
