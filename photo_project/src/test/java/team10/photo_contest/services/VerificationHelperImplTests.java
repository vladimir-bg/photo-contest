package team10.photo_contest.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import team10.photo_contest.exceptions.UnauthorizedOperationException;
import team10.photo_contest.repositories.contracts.ContestRepository;
import team10.photo_contest.repositories.contracts.EntryRepository;
import team10.photo_contest.repositories.contracts.ReviewsRepository;
import team10.photo_contest.repositories.contracts.UserRepository;
import team10.photo_contest.services.utils.VerificationHelperImpl;

import javax.persistence.EntityNotFoundException;

import static team10.photo_contest.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class VerificationHelperImplTests {
    @Mock
    UserRepository userRepository;

    @Mock
    ReviewsRepository reviewsRepository;

    @Mock
    ContestRepository contestRepository;

    @Mock
    EntryRepository entryRepository;

    @InjectMocks
    VerificationHelperImpl verificationHelper;

    @Test
    public void verifyUserCanView_Should_NotThrow() {
        //Arrange
        var user = createMockUser2();

        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(user);

        //Act
        verificationHelper.verifyUserCanView(0, user);

        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).getById(0);
    }

    @Test
    public void verifyUserCanView_Should_Throw() {
        //Arrange
        var user = createMockUser2();

        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(createMockUser1());

        //Act Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> verificationHelper.verifyUserCanView(0, user));
        Mockito.verify(userRepository, Mockito.times(1)).getById(0);
    }

    @Test
    public void verifyJuryCanModifyReview_Should_NotThrow() {
        //Arrange
        var user = createMockUser2();

        Mockito.when(reviewsRepository.getById(Mockito.anyInt())).thenReturn(createMockReview());
        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(user);

        //Act
        verificationHelper.verifyJuryCanModifyReview(createMockReview(), user);
        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).getById(1);
        Mockito.verify(reviewsRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void verifyJuryCanModifyReview_Should_Throw() {
        //Arrange
        var user = createMockUser2();

        Mockito.when(reviewsRepository.getById(Mockito.anyInt())).thenReturn(createMockReview());
        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(user);

        //Act Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> verificationHelper.verifyJuryCanModifyReview(createMockReview(), createMockUser1()));
        Mockito.verify(userRepository, Mockito.times(1)).getById(1);
        Mockito.verify(reviewsRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void verifyJuryCanModifyEntry_Should_NotThrow() {
        //Arrange
        var user = createMockUser2();

        Mockito.when(entryRepository.getById(Mockito.anyInt())).thenReturn(createMockEntry());
        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(user);

        //Act
        verificationHelper.verifyUserCanModifyEntry(createMockEntry(), user);
        //Assert
        Mockito.verify(userRepository, Mockito.times(1)).getById(1);
        Mockito.verify(entryRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void verifyJuryCanModifyEntry_Should_Throw() {
        //Arrange
        var user = createMockUser2();

        Mockito.when(entryRepository.getById(Mockito.anyInt())).thenReturn(createMockEntry());
        Mockito.when(userRepository.getById(Mockito.anyInt())).thenReturn(user);

        //Act Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> verificationHelper.verifyUserCanModifyEntry(createMockEntry(), createMockUser1()));
        Mockito.verify(userRepository, Mockito.times(1)).getById(1);
        Mockito.verify(entryRepository, Mockito.times(1)).getById(1);
    }

    @Test
    public void checkIfContestExists_Should_ReturnConte() {
        //Arrange
        var contest = createMockContest();
        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(contest);

        //Act Assert
        Assertions.assertEquals(contest, verificationHelper.checkIfContestExists(0));
        Mockito.verify(contestRepository, Mockito.times(1)).getById(0);
    }

    @Test
    public void checkIfContestExists_Should_Throw() {
        //Arrange
        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);

        //Act Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> verificationHelper.checkIfContestExists(0));
    }
}
