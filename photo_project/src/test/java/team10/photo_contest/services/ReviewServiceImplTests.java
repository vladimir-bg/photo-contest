package team10.photo_contest.services;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import team10.photo_contest.Helpers;
import team10.photo_contest.dtos.output.EntryViewDto;
import team10.photo_contest.exceptions.ContestNotReviewableException;
import team10.photo_contest.exceptions.DuplicateEntityException;
import team10.photo_contest.exceptions.EntityNotFoundException;
import team10.photo_contest.exceptions.UnauthorizedOperationException;
import team10.photo_contest.mapers.ReviewModelMapper;
import team10.photo_contest.models.Entry;
import team10.photo_contest.models.Review;
import team10.photo_contest.models.User;
import team10.photo_contest.repositories.contracts.ContestRepository;
import team10.photo_contest.repositories.contracts.EntryRepository;
import team10.photo_contest.repositories.contracts.ReviewsRepository;
import team10.photo_contest.services.utils.VerificationHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@ExtendWith(MockitoExtension.class)
public class ReviewServiceImplTests {

    @Mock
    ReviewsRepository reviewsRepository;
    @Mock
    EntryRepository entryRepository;
    @Mock
    ContestRepository contestRepository;

    @Mock
    VerificationHelper verificationHelper;
    @Mock
    ReviewModelMapper modelMapper;


    @InjectMocks
    ReviewServiceImpl reviewService;

    @Test
    public void update_Should_CallRepository_When_UserVerifiedCanModify() {
        // Arrange
        var mockCaller = Helpers.createMockUser2();
        var mockReview = Helpers.createMockReview();

        Mockito.doNothing().when(verificationHelper).verifyJuryCanModifyReview(mockReview, mockCaller);

        Mockito.when(reviewsRepository.update(mockReview)).thenReturn(mockReview);

        //Act
        reviewService.update(mockReview, mockCaller);

        //Assert
        Mockito.verify(reviewsRepository, Mockito.times(1))
                .update(Mockito.any(Review.class));
    }

    @Test
    public void viewEntriesForInvitedContests_Should_CallRepository_When_UserVerifiedAsJury() {
        // Arrange
        var mockJury = Helpers.createMockUser2();
        var mockScore = Helpers.createMockScore();
        mockJury.setUserScore(mockScore);

        var mockEntry1 = Helpers.createMockEntry();
        var mockEntry2 = Helpers.createMockEntry();
        mockEntry1.setContest(Helpers.createMockContest());
        mockEntry2.setContest(Helpers.createMockContest());
        List<Entry> list = new ArrayList<>();
        list.add(mockEntry1);
        list.add(mockEntry2);

        Mockito.when(reviewsRepository.viewEntriesForInvitedContests(mockJury)).thenReturn(list);

        //Act
        reviewService.viewEntriesForInvitedContests(mockJury);

        //Assert
        Mockito.verify(reviewsRepository, Mockito.times(1))
                .viewEntriesForInvitedContests(Mockito.any(User.class));
    }

    @Test
    public void viewEntriesForInvitedContests_Should_ThrowException_When_UserNotVerifiedAsJury() {
        // Arrange
        var mockJury = Helpers.createMockUser1();
        var mockScore = Helpers.createMockScore();
        mockScore.setUser(mockJury);
        mockJury.setUserScore(mockScore);

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> reviewService.viewEntriesForInvitedContests(mockJury));
    }

    @Test
    public void viewEntriesForInvitedContests_Should_ReturnDtoList_When_UserVerifiedAsJury() {
        // Arrange
        var mockJury = Helpers.createMockUser2();
        var mockScore = Helpers.createMockScore();
        mockJury.setUserScore(mockScore);

        var mockEntry1 = Helpers.createMockEntry();
        var mockEntry2 = Helpers.createMockEntry();
        mockEntry1.setContest(Helpers.createMockContest());
        mockEntry2.setContest(Helpers.createMockContest());
        mockEntry2.setEntryId(2);
        List<Entry> list = new ArrayList<>();
        list.add(mockEntry1);
        list.add(mockEntry2);

        Mockito.when(reviewsRepository.viewEntriesForInvitedContests(mockJury)).thenReturn(list);

        //Act
        List<EntryViewDto> result = reviewService.viewEntriesForInvitedContests(mockJury);

        //Assert
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals(mockEntry1.getEntryId(), result.get(0).getEntryId());
        Assertions.assertEquals(mockEntry2.getEntryId(), result.get(1).getEntryId());
    }

    @Test
    public void create_Should_ThrowException_When_EntryIsNotFound() {
        // Arrange
        var mockJury = Helpers.createMockUser1();
        var mockScore = Helpers.createMockScore();
        var mockReview = Helpers.createMockReview();
        mockScore.setUser(mockJury);
        mockJury.setUserScore(mockScore);

        Mockito.when(entryRepository.getById(mockReview.getReviewId())).thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> reviewService.create(mockReview, mockJury));
    }

    @Test
    public void create_Should_ThrowException_When_ContestIsNotFound() {
        // Arrange
        var mockJury = Helpers.createMockUser1();
        var mockScore = Helpers.createMockScore();
        var mockReview = Helpers.createMockReview();
        var mockContest = Helpers.createMockContest();
        var mockEntry = Helpers.createMockEntry();
        mockEntry.setContest(mockContest);
        mockScore.setUser(mockJury);
        mockJury.setUserScore(mockScore);

        Mockito.when(entryRepository.getById(mockReview.getReviewId())).thenReturn(mockEntry);
        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenThrow(EntityNotFoundException.class);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> reviewService.create(mockReview, mockJury));
    }

    @Test
    public void create_Should_ThrowException_When_JuryNotInvited() {
        // Arrange
        var mockJury = Helpers.createMockUser1();
        var mockScore = Helpers.createMockScore();
        var mockReview = Helpers.createMockReview();
        var mockContest = Helpers.createMockContest();
        var mockEntry = Helpers.createMockEntry();
        mockEntry.setContest(mockContest);
        mockScore.setUser(mockJury);
        mockJury.setUserScore(mockScore);

        Mockito.when(entryRepository.getById(Mockito.anyInt())).thenReturn(mockEntry);
        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(mockContest);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> reviewService.create(mockReview, mockJury));
    }

    @Test
    public void create_Should_ThrowException_When_PhaseNotCorrect() {
        // Arrange
        var mockJury = Helpers.createMockUser2();
        var mockScore = Helpers.createMockScore();
        var mockReview = Helpers.createMockReview();
        var mockContest = Helpers.createMockContest();
        var mockEntry = Helpers.createMockEntry();
        mockEntry.setContest(mockContest);
        mockScore.setUser(mockJury);
        mockJury.setUserScore(mockScore);

        Mockito.when(entryRepository.getById(Mockito.anyInt())).thenReturn(mockEntry);
        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(mockContest);
        Mockito.when(contestRepository.isJurorInContest(mockContest.getContestId(), mockJury.getUserId()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(ContestNotReviewableException.class,
                () -> reviewService.create(mockReview, mockJury));
    }

    @Test
    public void create_Should_ThrowException_When_JuryAlreadyReviewed() {
        // Arrange
        var mockJury = Helpers.createMockUser2();
        var mockScore = Helpers.createMockScore();
        var mockReview = Helpers.createMockReview();
        mockReview.setCreatedBy(mockJury.getUserId());
        var mockContest = Helpers.createMockContest();
        var mockEntry = Helpers.createMockEntry();
        mockEntry.setContest(mockContest);
        mockScore.setUser(mockJury);
        mockJury.setUserScore(mockScore);
        mockEntry.setReviews(Set.of(mockReview));
        mockContest.setPhaseITime(Helpers.calculateDays(0));

        Mockito.when(entryRepository.getById(Mockito.anyInt())).thenReturn(mockEntry);
        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(mockContest);
        Mockito.when(contestRepository.isJurorInContest(mockContest.getContestId(), mockJury.getUserId()))
                .thenReturn(true);
        Mockito.when(reviewsRepository.getByEntryAndJuror(mockEntry.getEntryId(), mockJury.getUserId()))
                .thenReturn(mockReview);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> reviewService.create(mockReview, mockJury));
    }

    @Test
    public void create_Should_CallRepository_When_NothingIsThrown() {
        // Arrange
        var mockJury = Helpers.createMockUser2();
        var mockScore = Helpers.createMockScore();
        var mockReview = Helpers.createMockReview();
        mockReview.setCreatedBy(mockJury.getUserId());
        var mockContest = Helpers.createMockContest();
        var mockEntry = Helpers.createMockEntry();
        mockEntry.setContest(mockContest);
        mockScore.setUser(mockJury);
        mockJury.setUserScore(mockScore);
        mockEntry.setReviews(Set.of(mockReview));
        mockContest.setPhaseITime(Helpers.calculateDays(0));

        Mockito.when(entryRepository.getById(Mockito.anyInt())).thenReturn(mockEntry);
        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(mockContest);
        Mockito.when(contestRepository.isJurorInContest(mockContest.getContestId(), mockJury.getUserId()))
                .thenReturn(true);
        Mockito.when(reviewsRepository.getByEntryAndJuror(mockEntry.getEntryId(), mockJury.getUserId()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        reviewService.create(mockReview, mockJury);

        //Assert
        Mockito.verify(reviewsRepository, Mockito.times(1))
                .create(Mockito.any(Review.class));
    }

}
