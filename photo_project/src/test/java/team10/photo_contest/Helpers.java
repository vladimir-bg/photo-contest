package team10.photo_contest;

import team10.photo_contest.models.*;

import java.util.*;

public class Helpers {

    public static Category createMockCategory() {
        var mockCategory = new Category();
        mockCategory.setCategoryId(1);
        mockCategory.setCategoryType("Puppies");
        return mockCategory;
    }

    public static Role createMockRoleUser() {
        var mockRole = new Role();
        mockRole.setRoleId(1);
        mockRole.setType("Photo Junkie");
        return mockRole;
    }

    public static Role createMockRoleOrganizer() {
        var mockRole = new Role();
        mockRole.setRoleId(2);
        mockRole.setType("Organizer");
        return mockRole;
    }

    public static User createMockUser1() {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setActive(true);
        mockUser.setUserRoles(Set.of(createMockRoleUser()));
        mockUser.setFirstName("Gosho");
        mockUser.setLastName("Georgiev");
        mockUser.setUserName("jorko1");
        mockUser.setPassword("12345678");
        return mockUser;
    }

    public static User createMockUser2() {
        var mockUser = new User();
        mockUser.setUserId(1);
        mockUser.setActive(true);
        mockUser.setUserRoles(Set.of(createMockRoleOrganizer()));
        mockUser.setFirstName("Pesho");
        mockUser.setLastName("Petrov");
        mockUser.setUserName("pesho1");
        mockUser.setPassword("12345678");
        return mockUser;
    }

    public static UserScore createMockScore() {
        var mockScore = new UserScore();
        User userToSet = createMockUser2();
        mockScore.setId(1);
        mockScore.setUser(userToSet);
        mockScore.setTotalScore(0);
        return mockScore;
    }


    public static Photo createMockPhoto() {
        var mockPhoto = new Photo();
        mockPhoto.setPhotoId(1);
        mockPhoto.setContestPhoto(false);
        mockPhoto.setUserId(createMockUser1().getUserId());
        return mockPhoto;
    }

    public static Contest createMockContest() {
        var mockContest = new Contest();
        mockContest.setContestId(1);
        mockContest.setTitle("Cats");
        mockContest.setOpen(true);
        mockContest.setCategories(createMockCategory());
        mockContest.setPhaseITime(calculateDays(2));
        mockContest.setPhaseIITime(calculateHours(calculateDays(2), 12));
        mockContest.setJury(Set.of(createMockUser2()));
        mockContest.setPhoto(createMockPhoto());
        mockContest.setContestEntries(Set.of(createMockEntry()));
        return mockContest;
    }

    public static Entry createMockEntry() {
        var mockEntry = new Entry();
        mockEntry.setEntryId(1);
        mockEntry.setTitle("Kotence");
        mockEntry.setStory("Kotence v parka");
        mockEntry.setUser(createMockUser1());
        mockEntry.setPhoto(createMockPhoto());
        return mockEntry;
    }

    public static Review createMockReview() {
        var mockReview = new Review();
        mockReview.setReviewId(1);
        mockReview.setComment("Good work");
        mockReview.setCreatedBy(createMockUser2().getUserId());
        mockReview.setScore(5);
        mockReview.setContestEntry(createMockEntry());
        return mockReview;
    }

    public static Date calculateDays(int intDate) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, intDate);
        return calendar.getTime();
    }

    public static Date calculateHours(Date date, int intHours) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, intHours);
        return calendar.getTime();
    }
}
